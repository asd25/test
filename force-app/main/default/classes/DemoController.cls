public with sharing class DemoController {
    /**
     * An empty constructor for the testing
     */
    public DemoController() {}

    /**
     * Get the version of the SFDX demo app
     */
    public String getAppVersion() {
        return '<script>alert(1);</script>';
    }
}
